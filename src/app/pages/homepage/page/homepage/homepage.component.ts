import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BaseService } from '../../../../core/base-service/service';
import { ChapterServicePathConst } from '../../shared/const';
import { ChapterModel } from '../../shared/model';

@Component({
	selector: 'app-homepage',
	templateUrl: './homepage.component.html',
	styleUrls: ['./homepage.component.scss'],
})
export class HomepageComponent implements OnInit, OnDestroy {
	private subscribers: Subscription[];

	public chapterData: ChapterModel[];

	constructor(private router: Router, private baseService: BaseService) {}

	ngOnInit() {
		this.subscribers = [];
		this.chapterData = [];

		this.getChapter();
	}

	ngOnDestroy() {
		this.subscribers.forEach(each => each.unsubscribe());
	}

	private getChapter() {
		const url = ChapterServicePathConst + '?language=id';

		const subs = this.baseService
			.getDataChapter(url, ChapterModel)
			.subscribe(resp => {
				this.chapterData = resp;
			});

		this.subscribers.push(subs);
	}

	public chapterChanges(chapter: ChapterModel) {
		this.router.navigate(['./surah'], {
			queryParams: {
				surahId: chapter.id,
			},
		});
	}
}
