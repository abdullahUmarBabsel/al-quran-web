import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChapterModel } from '../../shared/model';

@Component({
	selector: 'app-chapter-card',
	templateUrl: './chapter-card.component.html',
	styleUrls: ['./chapter-card.component.scss'],
})
export class ChapterCardComponent implements OnInit {
	@Input()
	public chapter: ChapterModel;

	@Output()
	public chapterChanges: EventEmitter<ChapterModel>;

	constructor() {
		this.chapterChanges = new EventEmitter(null);
	}

	ngOnInit() {}

	public chapterDetail(chapter: ChapterModel) {
		if (chapter) {
			this.chapterChanges.emit(chapter);
		}
	}
}
