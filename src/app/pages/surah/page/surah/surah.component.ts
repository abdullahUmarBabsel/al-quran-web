import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatAdvancedAudioPlayerComponent, Track } from 'ngx-audio-player';
import { Subscription } from 'rxjs';
import { AudioPlayerConfigConst } from '../../../../app-config.const';
import { BaseService } from '../../../../core/base-service/service';
import { SurahServicePathConst } from '../../shared/const';
import {
	SurahMetaModel,
	SurahModel,
	SurahVersesModel,
} from '../../shared/model';

@Component({
	selector: 'app-surah',
	templateUrl: './surah.component.html',
	styleUrls: ['./surah.component.scss'],
})
export class SurahComponent implements OnInit, OnDestroy {
	private subscribers: Subscription[];
	private currentTrack: Track;
	private lastAyat: boolean;
	private surahId: string;

	public isLoading: boolean;
	public throttle: number;
	public playlist: Track[];
	public playerConfig: any;
	public scrollDistance: number;
	public scrollUpDistance: number;
	public surah: SurahVersesModel[];
	public surahMeta: SurahMetaModel;
	public arrAyat: SurahVersesModel[];

	@ViewChild('advanced', { static: false })
	advancedPlayer: MatAdvancedAudioPlayerComponent;

	constructor(
		private activatedRoute: ActivatedRoute,
		private baseService: BaseService
	) {}

	ngOnInit() {
		this.surah = [];
		this.arrAyat = [];
		this.playlist = [];
		this.subscribers = [];
		this.throttle = 100;
		this.scrollDistance = 1;
		this.scrollUpDistance = 2;
		this.lastAyat = false;
		this.isLoading = false;
		this.currentTrack = null;

		this.surahMeta = new SurahMetaModel();
		this.playerConfig = AudioPlayerConfigConst;

		this.getSuratId();
	}

	ngOnDestroy() {
		this.subscribers.forEach(each => each.unsubscribe());
	}

	private getSurah(offset: number) {
		const url =
			SurahServicePathConst.replace('{id}', this.surahId) +
			'?offset=' +
			offset +
			'&limit=10&translations=33&language=id&recitation=1';

		const subs = this.baseService
			.getDataSurah(url, SurahModel)
			.subscribe(resp => {
				this.surah = resp.verses;
				this.surahMeta = resp.meta;

				this.arrAyat = [...this.arrAyat, ...this.surah];
				this.playlist = [
					...this.playlist,
					...this.mergeAudio(this.surah),
				];

				this.appendTracksToPlaylist();
			});

		this.subscribers.push(subs);
	}

	private getSuratId() {
		this.isLoading = true;

		this.activatedRoute.queryParams.subscribe(resp => {
			if (resp) {
				this.surah = [];
				this.arrAyat = [];
				this.playlist = [];
				this.surahId = resp['surahId'];

				this.getSurah(0);
				this.isLoading = false;
			}
		});
	}

	private mergeAudio(audio: SurahVersesModel[]) {
		const arrAudio = audio.reduce((result, each, currentIndex) => {
			each.audio.index = currentIndex + 1;
			result.push(each.audio);
			return result;
		}, []);

		return arrAudio;
	}

	private logCurrentTrack() {
		const subs = this.advancedPlayer.audioPlayerService
			.getCurrentTrack()
			.subscribe(track => {
				this.currentTrack = track;

				if (this.currentTrack.index === this.playlist.length) {
					this.lastAyat = true;
				}
			});

		this.subscribers.push(subs);
	}

	private appendTracksToPlaylist() {
		this.advancedPlayer.audioPlayerService.setPlaylist(this.playlist);
	}

	private resetTrack() {
		this.advancedPlayer.resetSong();
	}

	public onEnded() {
		this.currentTrack = null;

		this.logCurrentTrack();

		if (this.lastAyat) {
			this.resetTrack();
		}
	}

	public onScrollDown() {
		const start = this.arrAyat.length;

		if (this.surahMeta.totalCount > start) {
			this.getSurah(start);
		}
	}
}
