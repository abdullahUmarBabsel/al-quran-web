import { environment } from '../../../../../environments/environment';

const surahServicePath = 'chapters/{id}/verses';

export const SurahServicePathConst = `${environment.apiV3}/${surahServicePath}`;
