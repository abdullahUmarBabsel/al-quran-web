export class SurahTranslateModel {
	public id: number;
	public languageName: string;
	public resourceId: number;
	public resourceName: string;
	public text: string;

	public convert(dto: any): SurahTranslateModel {
		if (dto.id != null) {
			this.id = dto.id;
		}
		if (dto.language_name != null) {
			this.languageName = dto.language_name;
		}
		if (dto.resource_id != null) {
			this.resourceId = dto.resource_id;
		}
		if (dto.resource_name != null) {
			this.resourceName = dto.resource_name;
		}
		if (dto.text != null) {
			this.text = dto.text;
		}

		return this;
	}
}
