export class SurahMediaModel {
	public authorName: string;
	public embedText: string;
	public provide: string;
	public url: string;

	public convert(dto: any): SurahMediaModel {
		if (dto.author_name !== null) {
			this.authorName = dto.author_name;
		}
		if (dto.embed_text !== null) {
			this.embedText = dto.embed_text;
		}
		if (dto.provide !== null) {
			this.provide = dto.provide;
		}
		if (dto.url !== null) {
			this.url = dto.url;
		}

		return this;
	}
}
