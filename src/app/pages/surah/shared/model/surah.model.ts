import { SurahMetaModel } from './surah-meta.model';
import { SurahVersesModel } from './surah-verses.model';

export class SurahModel {
	public meta: SurahMetaModel;
	public verses: SurahVersesModel[];

	public convert(dto: any): SurahModel {
		if (dto.meta !== null) {
			this.meta = new SurahMetaModel().convert(dto.meta);
		}
		if (dto.verses !== null) {
			this.verses = dto.verses.reduce((result, each) => {
				result.push(new SurahVersesModel().convert(each));
				return result;
			}, []);
		}

		return this;
	}
}
