import { SurahAudioModel } from './surah-audio.model';
import { SurahMediaModel } from './surah-media.model';
import { SurahTranslateModel } from './surah-translate.model';

export class SurahVersesModel {
	id: number;
	chapterId: number;
	hizbNumber: number;
	juzNumber: number;
	pageNumber: number;
	rubNumber: number;
	sajdah: string;
	sajdahNumber: number;
	textIndopak: string;
	textMadani: string;
	textSimple: string;
	verseKey: string;
	verseNumber: number | string;
	audio: SurahAudioModel;
	mediaContents: SurahMediaModel;
	translations: SurahTranslateModel;

	public convert(dto: any): SurahVersesModel {
		if (dto.id != null) {
			this.id = dto.id;
		}
		if (dto.chapter_id !== null) {
			this.chapterId = dto.chapter_id;
		}
		if (dto.hizb_number != null) {
			this.hizbNumber = dto.hizb_number;
		}
		if (dto.juz_number != null) {
			this.juzNumber = dto.juz_number;
		}
		if (dto.page_number != null) {
			this.pageNumber = dto.page_number;
		}
		if (dto.rub_number != null) {
			this.rubNumber = dto.rub_number;
		}
		if (dto.sajdah != null) {
			this.sajdah = dto.sajdah;
		}
		if (dto.sajdah_number != null) {
			this.sajdahNumber = dto.sajdah_number;
		}
		if (dto.text_indopak != null) {
			this.textIndopak = dto.text_indopak;
		}
		if (dto.text_madani != null) {
			this.textMadani = dto.text_madani;
		}
		if (dto.text_simple != null) {
			this.textSimple = dto.text_simple;
		}
		if (dto.verse_key != null) {
			this.verseKey = dto.verse_key;
		}
		if (dto.verse_number != null) {
			this.verseNumber = this.convertPersianNumber(dto.verse_number);
		}
		if (dto.audio !== null) {
			this.audio = new SurahAudioModel().convert(dto.audio);
		}
		if (dto.media_contents !== null) {
			this.mediaContents = new SurahMediaModel().convert(
				dto.media_contents
			);
		}
		if (dto.translations) {
			this.translations = new SurahTranslateModel().convert(
				dto.translations[0]
			);
		}

		return this;
	}

	private convertPersianNumber(value: number) {
		const arabic = '٠١٢٣٤٥٦٧٨٩';
		const digit = String(value).toString().replace('.', ',');

		return digit.replace(/[0-9]/g, function (w) {
			return arabic[w];
		});
	}
}
