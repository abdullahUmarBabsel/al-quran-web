import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ChapterModel } from '../../../pages/homepage/shared/model';
import { SurahModel } from '../../../pages/surah/shared/model';
import { generateHttpParams } from '../../util';

@Injectable({
	providedIn: 'root',
})
export class BaseService {
	constructor(private http: HttpClient) {}

	public getDataChapter(
		url: string,
		responseModel: any,
		requestParamModel?: any
	): Observable<ChapterModel[]> {
		const params = requestParamModel
			? generateHttpParams(requestParamModel.convert())
			: null;

		return this.http.get(url, { params }).pipe(
			map((model: any): any => model.chapters),
			map((model: any): ChapterModel[] => {
				return this.mapArrayData(model, responseModel);
			})
		);
	}

	public getDataSurah(
		url: string,
		responseModel: any,
		requestParamModel?: any
	): Observable<SurahModel> {
		const params = requestParamModel
			? generateHttpParams(requestParamModel.convert())
			: null;

		return this.http.get(url, { params }).pipe(
			map(
				(model: any): SurahModel => {
					return this.mapObjectData(model, responseModel);
				}
			)
		);
	}

	private mapObjectData(dto: any, responseModel: any) {
		if (Object.entries(dto).length === 0) {
			return null;
		}

		const dataModel = new responseModel().convert(dto);
		return dataModel;
	}

	private mapArrayData(dtos: any[], responseModel: any) {
		const dataModel = dtos.reduce((result, each) => {
			const model = new responseModel();
			result.push(model.convert(each));

			return result;
		}, []);

		return dataModel;
	}
}
