export const AudioPlayerConfigConst = {
	autoPlay: false,
	expanded: true,
	displayTitle: false,
	displayPlayList: false,
	displayVolumeControls: true,
	disablePositionSlider: true,
};
