import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		loadChildren: () =>
			import('./layout/layout.module').then(m => m.LayoutModule),
	},
	{
		path: 'not-found',
		loadChildren: () =>
			import('./pages/not-found/not-found.module').then(
				m => m.NotFoundModule
			),
	},
	{
		path: '**',
		redirectTo: '/not-found',
		pathMatch: 'full',
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			useHash: true,
			scrollPositionRestoration: 'top',
		}),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
