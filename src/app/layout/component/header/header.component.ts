import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSelectChange } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BaseService } from '../../../core/base-service/service';
import { ChapterServicePathConst } from '../../../pages/homepage/shared/const';
import { ChapterModel } from '../../../pages/homepage/shared/model';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
	private subscribers: Subscription[];

	public surahId: FormControl;
	public chapterData: ChapterModel[];

	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private baseService: BaseService
	) {}

	ngOnInit() {
		this.subscribers = [];
		this.surahId = new FormControl(null);

		this.getChapter();
		this.getSuratId();
	}

	ngOnDestroy() {
		this.subscribers.forEach(each => each.unsubscribe());
	}

	private getSuratId() {
		this.activatedRoute.queryParams.subscribe(resp => {
			if (resp) {
				this.surahId.setValue(Number(resp['surahId']));
			}
		});
	}

	private getChapter() {
		const url = ChapterServicePathConst + '?language=id';

		const subs = this.baseService
			.getDataChapter(url, ChapterModel)
			.subscribe(resp => {
				this.chapterData = resp;
			});

		this.subscribers.push(subs);
	}

	public navigateToHomepage() {
		this.router.navigate(['/']);
	}

	public navigateToSurah(surah: MatSelectChange) {
		if (surah.value > 0) {
			this.surahId.setValue(surah.value);

			this.router.navigate(['./surah'], {
				queryParams: {
					surahId: surah.value,
				},
			});
		} else {
			this.getSuratId();
		}
	}
}
